
const fn = require('./functions.js');
const repo = require('./repo.js');
const express = require('express');

const app = express();
app.use(express.json());

app.get('/', (req, res) => {
    res.send("Welcome to Vidly REST API Endpoint");
});

app.get('/about', (req, res) => {
    res.send("About Vidly");
});

app.get('/api/genres', (req, res) => {
    return res.send(repo.getAllGenres());
});

app.get('/api/genres/:id', (req, res) => {
    const genre = repo.getSingleGenre(req.params.id);
    if(genre) return res.send(genre);
    else return res.status(404).send("Invalid Genre ID");
});

app.post('/api/genres', (req, res) => {
    const {error} = fn.validateGenre(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    const genres = repo.getAllGenres();

    const id = (repo.total() == 0) ? 1 : (genres[repo.total()-1].id + 1);
    const genre = {
        id: id,
        name: req.body.name
    };
    repo.addGenre(genre);
    return res.send(genre);
});

app.put('/api/genres/:id', (req, res) => {
    const genre = repo.getSingleGenre(req.params.id);
    if(!genre) return res.status(404).send("Invalid Genre ID");

    const {error} = fn.validateGenre(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    genre.name = req.body.name;
    repo.updateGenre(genre.id, genre);
    return res.send(genre);

});

app.delete('/api/genres/:id', (req, res) => {
    const genre = repo.getSingleGenre(req.params.id);
    if(!genre) return res.status(404).send("Invalid Genre ID");

    repo.deleteGenre(genre.id);
    return res.send(genre);
});

app.listen(3000, () => console.log("Listening at 3000"));