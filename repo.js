const dbname = 'genres.json';
const fs = require('fs');

const getAllGenres = () => {
    return require(`./${dbname}`);
};

const getSingleGenre = (id) => {
    const genres = getAllGenres();
    return genres.find(g => g.id === parseInt(id));
};

const total = () => {
    const genres = getAllGenres();
    return genres.length;
};

const addGenre = (genre) => {
    let genres = getAllGenres();
    genres.push(genre);
    const jsonContent = JSON.stringify(genres);
    fs.writeFile(dbname, jsonContent, 'utf8', function (err) {
        console.log(err);
        if (err != null) {
            console.log(`An error occured while writing JSON Object to File: ${err}`);
        }
    });
};

const updateGenre = (id, genre) => {
    let genres = getAllGenres();
    const oldGenre = getSingleGenre(id);
    genres[genres.indexOf(oldGenre)] = genre;
    const jsonContent = JSON.stringify(genres);
    fs.writeFile(dbname, jsonContent, 'utf8', function (err) {
        if (err) {
            console.log(`An error occured while writing JSON Object to File: ${err}`);
        }
    });
};

const deleteGenre = (id) => {
    let genres = getAllGenres();
    const oldGenre = getSingleGenre(id);
    genres.splice(genres.indexOf(oldGenre), 1);
    const jsonContent = JSON.stringify(genres);
    fs.writeFile(dbname, jsonContent, 'utf8', function (err) {
        if (err) {
            console.log(`An error occured while writing JSON Object to File: ${err}`);
        }
    });
};

module.exports = {
    getAllGenres,
    getSingleGenre,
    total,
    addGenre,
    updateGenre,
    deleteGenre
};